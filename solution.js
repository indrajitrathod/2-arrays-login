const userLoginData = require('./loginData');

const fetchAgender = (userLoginData) => {
    if (userLoginData === undefined) {
        return [];
    }

    const agenders = userLoginData.filter((user) => {
        return user.gender.toLowerCase().includes('agender');
    });
    return agenders;
}

const addFullName = (userLoginData) => {
    if (userLoginData === undefined) {
        return [];
    }

    const users = userLoginData.map((user) => {
        return { full_name: `${user.first_name} ${user.last_name}`, ...user, }
    });
    return users;
}

const extractOrgEmailUsers = (userLoginData) => {
    if (userLoginData === undefined) {
        return [];
    }

    const orgEmailusers = userLoginData.filter((user) => {
        return user.email.match(/.org$/);
    });
    return orgEmailusers;
}

const sortFirstNameDescend = (userLoginData) => {
    if (userLoginData === undefined) {
        return [];
    }

    const usersFirstNameDescended = userLoginData.sort((userA, userB) => {
        return userB.first_name > userA.first_name ? 1 : -1;
    });
    return usersFirstNameDescended;
}

const splitIpComponents = (userLoginData) => {
    if (userLoginData === undefined) {
        return [];
    }

    const users = userLoginData.map((user) => {

        let ipComponents = user.ip_address.split('.');
        ipComponents = ipComponents.map((eachComponent) => {
            return parseInt(eachComponent);
        });

        return { ...user, ipComponents: ipComponents };
    });

    return users;
}

const sumofSecondComponents = (userLoginData) => {
    if (userLoginData === undefined) {
        return [];
    }

    users = splitIpComponents(userLoginData);

    const sum = users.reduce((sum, user) => {
        return sum += user.ipComponents[1];
    },0);

    return sum;
}

const sumofFourthComponents = (userLoginData) => {
    if (userLoginData === undefined) {
        return [];
    }

    users = splitIpComponents(userLoginData);

    const sum = users.reduce((sum, user) => {
        return sum += user.ipComponents[3];
    },0);

    return sum;
}
